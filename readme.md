## Servidor json

dentro da pasta do projeto ionic rodar

```
git clone https://gitlab.com/maurouberti/json-server
```

instalar plugin

```
npm install json-server --save-dev
```

rodar servidor

```
node json-server/server.js
```

no browser utilizar **http://localhost:3000/products**

a **url** no ionic **/products** 

> o arquivo json com os dados é o json-server/db.json

## Referências

> https://github.com/typicode/json-server
> https://www.json-generator.com/